﻿namespace StringCalculatorTwoV2
{
    public interface ISubtraction
    {
        int Subtract(string numbers);
    }
}